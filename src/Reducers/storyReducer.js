import { storyType } from "../Actions/Type/storyType";
// initial Value of the state
const initialState = {
  data: [],
};
// Reducer for store
export const storyReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case storyType:
      return {
        ...state,
        data: state.data.concat(payload),
      };
    default:
      return state;
  }
};
