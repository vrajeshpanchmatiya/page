import { Button } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import { Link } from "react-router-dom";
import { postStoryAction } from "../Actions/postStoryAction";
import "../Common.scss";
const PostStoryFetch = () => {
  const [id, setId] = useState(0);
  const dispatch = useDispatch();
  // useEffect for update id every 10 Seconds
  useEffect(() => {
    setTimeout(() => {
      dispatch(postStoryAction(id));
      setId(id + 1);
    }, 10000);
  }, [dispatch, id]);
  // useSelector for calling data
  const detail = useSelector((state) => {
    return state.data;
  });

  // Table Field Header
  const columns = [
    {
      Header: "TITLE",
      accessor: "title",
      style: { backgroundColor: "aliceblue" },
    },
    {
      Header: "URL",
      accessor: "url",
      sortable: false,
      filterable: false,
      style: { backgroundColor: "lightblue" },
    },
    {
      Header: "CREATED_AT",
      accessor: "created_at",
      style: { backgroundColor: "lightgreen" },
    },
    {
      Header: "AUTHOR",
      accessor: "author",
      style: { backgroundColor: "salmon" },
    },
    {
      Header: "ACTION",
      style: { backgroundColor: "lightgrey" },
      Cell: (props) => {
        return (
          <Link
            to={{ pathname: "/OriginalStory", data: props.original }}
            className="link"
          >
            <Button type="submit" variant="outlined" color="secondary">
              Fetch Row
            </Button>
          </Link>
        );
      },
    },
  ];
  // React Table shows the data in table
  return (
    <ReactTable
      columns={columns}
      noDataText="Please Wait for a Seconds"
      defaultPageSize={20}
      showPageSizeOptions={false}
      filterable
      data={detail}
    />
  );
};
export default PostStoryFetch;
