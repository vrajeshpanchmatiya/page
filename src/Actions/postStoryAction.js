import { storyType } from "./Type/storyType";
import { storyApi } from "../Services/storyApi";
// Action for storyApi
export const postStoryAction = (id) => {
  return async (dispatch) => {
    const detail = await storyApi(id);
    dispatch({ type: storyType, payload: detail.data.hits });
  };
};
