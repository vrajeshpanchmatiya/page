import axios from "axios";

export const storyApi = (id) => {
  // Api calling
  return axios.get(`${process.env.REACT_APP_API_URL}${id}`);
};
