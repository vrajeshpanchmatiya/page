import React from "react";
const OriginalStory = (props) => {
  // passing data with the help of props
  const details = props.location.data;
  // JSON file of the selected row
  return <div>{JSON.stringify(details)}</div>;
};
export default OriginalStory;
