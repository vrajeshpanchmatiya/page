import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import PostStoryFetch from "./Components/PostStoryFetch";
import OriginalStory from "./Components/OriginalStory";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={PostStoryFetch} />
          <Route path="/OriginalStory" component={OriginalStory} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
